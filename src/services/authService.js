import http from "./httpService";
import { apiUrl } from "../config.json";

export const UserName = "UserName";
export const UserId = "UserId";
export const Customers = "Customers";

export async function login(username, password) {
  return http
    .post(`${apiUrl}/api/login`, { username: username })
    .then((res) => {
      console.table(res.data);
      return res.data;
    })
    .catch((error) => {
      console.error(error);
    });
}

export function getCurrentUser() {
  try {
    const username = localStorage.getItem(UserName);
    return username;
  } catch (ex) {
    return null;
  }
}

export function getCurrentUserId() {
  try {
    const result = localStorage.getItem(UserId);
    return result;
  } catch (ex) {
    return null;
  }
}

export function logout() {
  localStorage.removeItem(UserName);
  localStorage.removeItem(UserId);
  localStorage.removeItem(Customers);
}

export default {
  login,
  logout,
  getCurrentUser,
};

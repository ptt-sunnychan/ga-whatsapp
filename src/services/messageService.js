import http from "./httpService";
import { apiUrl } from "../config.json";

export async function getMsgHistory(userId) {
  try {
    const res = await http.get(`${apiUrl}/api/whatsapp/msgHistory`, {
      params: { userId: userId },
    });
    console.table(res.data);
    return res.data;
  } catch (error) {
    console.error(error);
  }
}

export function sendMessage(message) {
  if (message) {
    const body = { ...message };
    return http.post(apiUrl + "/api/whatsapp/sendmsg", body);
  }

  return;
}

export default {
  getMsgHistory,
  sendMessage
};

import React from "react";
import Joi from "joi-browser";
import Form from "./common/form";
import { login, UserName, UserId, Customers } from "../services/authService";

class LoginForm extends Form {
  state = {
    data: { username: "", password: "" },
    errors: {},
  };

  schema = {
    username: Joi.string().required().label("Username"),
    password: Joi.string().required().label("Password"),
  };

  doSubmit = async () => {
    try {
      // Call the server
      const result = await login(
        this.state.data.username,
        this.state.data.password
      );
      console.log(result);
      localStorage.setItem(UserName, this.state.data.username);
      localStorage.setItem(UserId, result.id);
      localStorage.setItem(Customers, JSON.stringify(result.customers));
      const { state } = this.props.location;
      window.location = state ? state.from.pathname : "/chatroom";
    } catch (ex) {
      if (ex.response && ex.response.status === 400) {
        const errors = { ...this.state.errors };
        errors.username = ex.response.data;
        this.setState({ errors });
      }
    }
  };

  render() {
    return (
      <div className={"container"} style={{ marginTop: "30px" }}>
        <h1>Login</h1>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput("username", "Username")}
          {this.renderInput("password", "Password", "password")}
          {this.renderButton("Login")}
        </form>
      </div>
    );
  }
}

export default LoginForm;

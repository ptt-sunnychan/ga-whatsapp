import React from "react";
import auth from "../services/authService";
import { Link, NavLink } from "react-router-dom";
import { logout } from "../services/authService";

const NavBar = ({ user }) => {

  console.log(user.user);
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <Link className="navbar-brand" to="/">
        Giorgio Armani
      </Link>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarNavAltMarkup"
        aria-controls="navbarNavAltMarkup"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon" />
      </button>
      <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div className="navbar-nav">
          {(user.user == null || user.user == "") && (
            <React.Fragment>
              <NavLink className="nav-item nav-link" to="/login">
                Login
              </NavLink>
              <NavLink className="nav-item nav-link" to="/register">
                Register
              </NavLink>
            </React.Fragment>
          )}
          {(user.user != "" && user.user != null) && (
            <React.Fragment>
              <NavLink className="nav-item nav-link" to="/chatroom">
                {user.user}
              </NavLink>
              <NavLink className="nav-item nav-link" onClick={() => logout()} to="/login">
                Logout
              </NavLink>
            </React.Fragment>
          )}
        </div>
      </div>
    </nav>
  );
};

export default NavBar;

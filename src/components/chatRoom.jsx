import React, { Component } from "react";
import styles from "@chatscope/chat-ui-kit-styles/dist/default/styles.min.css";
import {
  MainContainer,
  ChatContainer,
  MessageList,
  Message,
  MessageInput,
  Sidebar,
  Search,
  ConversationList,
  ConversationHeader,
  Conversation,
  Avatar,
} from "@chatscope/chat-ui-kit-react";
import { getCurrentUserId, Customers } from "../services/authService";
import { getMsgHistory, sendMessage } from "../services/messageService";
// import bootstrap from 'bootstrap';

// import "./../App.css";

const Modal = () => {
  return (
    <div id="myModal" class={`modal`}>
      <div class="modal-content">
        <span class="close">&times;</span>
        <p>Some text in the Modal..</p>
      </div>
    </div>
  );
};

class chatRoom extends Component {
  intervalID;

  constructor(props) {
    super(props);

    this.state = {
      customersArr: [],
      messages: [],
      activeConversationUser: null,
    };
  }

  componentDidMount() {
    // const genres = [
    //   { message: "demo", sender: "All Genres", direction: "outgoing" },
    //   { message: "demo1", sender: "All Genres", direction: "outgoing" },
    //   { message: "demo2", sender: "All Genres", direction: "outgoing" },
    // ];
    let customers = this.getCustomersFromStorage();
    this.setState({
      customersArr: customers.map((i) => {
        return {
          ...i,
          avatar:
            "https://chatscope.io/storybook/react/static/media/joe.641da105.svg",
        };
      }),
    });

    this.getMessage();
  }

  componentWillUnmount() {
    console.log("unmount");
    clearTimeout(this.intervalID);
  }

  onChange = (props) => {};

  onSubmit = (props) => {
    const msg = {
      message: props,
      sender: this.state.activeConversationUser.name,
      direction: "outgoing",
      customerId: this.state.activeConversationUser.id,
    };
    const msgParams = {
      userId: getCurrentUserId(),
      customerId: this.state.activeConversationUser.id,
      msg: props,
    };
    this.setState({ messages: [...this.state.messages, msg] }, () => {
      console.log(this.state.messages);
      //   {
      //     "userId": 2,
      //     "customerId": 3,
      //     "msg": "This is a test from postman!"
      // }
      sendMessage(msgParams);
    });
  };

  getCustomersFromStorage() {
    try {
      return JSON.parse(localStorage.getItem(Customers));
    } catch (ex) {
      return null;
    }
  }

  getMessage() {
    console.log("get message");
    getMsgHistory(getCurrentUserId()).then((res) => {
      console.log(res);
      //   { message: "demo", sender: "All Genres", direction: "outgoing" },

      // api return : {
      //   "userId": 2,
      //   "userName": "Susan",
      //   "customerId": 3,
      //   "customerName": "Peter",
      //   "customerPhone": "+85290763691",
      //   "msg": "This is a test4!",
      //   "msgTime": "2021-07-22T04:48:39.389Z",
      //   "direction": "outgoing",
      //   "status": 3
      // }
      if (res && res.length > 0) {
        this.setState({
          messages: res.map((i) => {
            return {
              ...i,
              message: i.msg,
              sender: i.userName,
            };
          }),
        });
      }
      this.intervalID = setTimeout(this.getMessage.bind(this), 5000);
    });
  }

  render() {
    return (
      <div style={{ position: "relative", height: "93vh" }}>
        {/* <Modal /> */}
        <MainContainer>
          <Sidebar position="left" scrollable={false}>
            <Search placeholder="Search..." />
            <ConversationList>
              {this.state.customersArr &&
                this.state.customersArr.map((user, key) => {
                  return (
                    <Conversation
                      key={key}
                      name={user.name}
                      // lastSenderName={user.name}
                      info="Last: 22/07/2021@Harbour City"
                      onClick={() => {
                        this.setState({
                          activeConversationUser: user,
                        });
                      }}
                    >
                      <Avatar
                        key={key}
                        src={user.avatar}
                        name={user.name}
                        status="available"
                      />
                    </Conversation>
                  );
                })}
            </ConversationList>
          </Sidebar>
          {this.state.activeConversationUser && (
            <ChatContainer>
              <ConversationHeader
                data-toggle="modal"
                data-target="#myModal"
                onClick={() =>
                  window.alert(`${this.state.activeConversationUser.phone}`)
                }
              >
                <ConversationHeader.Back />
                <Avatar
                  src={this.state.activeConversationUser.avatar}
                  name={this.state.activeConversationUser.name}
                />
                <ConversationHeader.Content
                  userName={this.state.activeConversationUser.name}
                  // info={"tel: " + this.state.activeConversationUser.phone}
                />
              </ConversationHeader>
              <MessageList>
                {this.state.messages
                  .filter(
                    (item) =>
                      item.customerId === this.state.activeConversationUser.id
                  )
                  .map((msg, key) => {
                    return (
                      <Message
                        key={key}
                        model={{
                          message: msg.message,
                          sentTime: "just now",
                          sender: "Joe",
                          position: "single",
                          direction: msg.direction,
                        }}
                      />
                    );
                  })}
              </MessageList>
              <MessageInput
                placeholder="Type message here"
                onChange={this.onChange}
                onSend={this.onSubmit}
              />
            </ChatContainer>
          )}
        </MainContainer>
      </div>
    );
  }
}

export default chatRoom;

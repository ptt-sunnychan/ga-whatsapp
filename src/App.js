import React, { Component } from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import NotFound from "./components/notFound";
import NavBar from "./components/navBar";
import LoginForm from "./components/loginForm";
import RegisterForm from "./components/registerForm";
import chatRoom from './components/chatRoom';
import auth from "./services/authService";
import "react-toastify/dist/ReactToastify.css";
import "./App.css";


class App extends Component {
  state = {
    user: "",
  }

  componentDidMount() {
    const user = auth.getCurrentUser();
    this.setState({ user: user });
    if (this.state.user == null || this.state.user == "") {
      console.log("object");
    }
    else { }
  }
  render() {
    console.log(this.state.user);
    return (
      <React.Fragment>
        <ToastContainer />
        <NavBar user={this.state} />
        <main>
          <Switch>
            <Route path="/register" component={RegisterForm} />
            <Route path="/login" component={LoginForm} />
            <Route path="/chatroom" component={chatRoom} />
            <Route path="/not-found" component={NotFound} />
            <Redirect path="/" to={this.state.user == "" || this.state.user == null ? "/login" : "/chatroom"} />
            <Redirect to="/not-found" />
          </Switch>
        </main>
      </React.Fragment>
    );
  }
}

export default App;
